//
// Created by gdhsnlvr on 03.07.17.
//

#include "Food.h"

Food::Food(int x, int y, FoodType type, QPixmap *pixmap) : GridItem(x, y, pixmap) {
    _type = type;
}

FoodType Food::get_type() const {
    return _type;
}
