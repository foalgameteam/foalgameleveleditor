//
// Created by gdhsnlvr on 03.07.17.
//

#include "SceneGraphicsView.h"
#include "GridItemBuilder.h"

#include <QMouseEvent>
#include <QDebug>

#include <cstdlib>
#include <cmath>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>

SceneGraphicsView::SceneGraphicsView() {
    _scene.setItemIndexMethod(QGraphicsScene::NoIndex);

    setScene(&_scene);
    setRenderHint(QPainter::Antialiasing);
    setCacheMode(QGraphicsView::CacheBackground);
    setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    setWindowTitle(QT_TRANSLATE_NOOP(QGraphicsView, "Foal game editor"));
    setBackgroundBrush(Qt::darkGray);
    resize(400, 300);

    _width = 8;
    _height = 32;

    _gridItemBuilder.setPixmap(CellType::DEFAULT_CELL, new QPixmap(":/resource/default_cell.png"));
    _gridItemBuilder.setPixmap(CellType::EMPTY_CELL, new QPixmap(":/resource/empty_cell.png"));
    _gridItemBuilder.setPixmap(CellType::TIMED_CELL, new QPixmap(":/resource/timed_cell.png"));
    _gridItemBuilder.setPixmap(CellType::MOVING_CELL, new QPixmap(":/resource/moving_cell.png"));
    _gridItemBuilder.setPixmap(CellType::SURPRISE_CELL, new QPixmap(":/resource/surprise_cell.png"));

    _gridItemBuilder.setPixmap(BonusType::HEALTH_BONUS, new QPixmap(":/resource/health_bonus.png"));
    _gridItemBuilder.setPixmap(BonusType::TIME_SLOW_BONUS, new QPixmap(":/resource/time_slow_bonus.png"));
    _gridItemBuilder.setPixmap(BonusType::ROCKET_BONUS, new QPixmap(":/resource/rocket_bonus.png"));

    _gridItemBuilder.setPixmap(FoodType::FOOD_1, new QPixmap(":/resource/food_1.png"));
    _gridItemBuilder.setPixmap(FoodType::FOOD_2, new QPixmap(":/resource/food_2.png"));
    _gridItemBuilder.setPixmap(FoodType::FOOD_3, new QPixmap(":/resource/food_3.png"));

    _mouse_pressed = false;
    _current_cell_type = COUNT_CELL;
    _current_bonus_type = COUNT_BONUS;
    _current_food_type = COUNT_FOOD;

    generate();
}

void SceneGraphicsView::generate() {
    _scene.clear();
    _scene.setSceneRect(- 32, - 32, _width * 32 + 64, _height * 32 + 64 + 32);
    setMinimumWidth(_width * 32 + 32 * 3);

    for (int i = 0; i < _width; ++i) {
        for (int j = 0; j < _height; ++j) {
            CellType cellType = static_cast<CellType> (rand() % CellType::COUNT_CELL);
            Cell *cell = _gridItemBuilder.buildCell(i, j, cellType);
            _scene.addItem(cell);

            if (cellType == EMPTY_CELL) continue;

            double probability = qrand() * 1.0 / RAND_MAX;
            if (probability < 0.1) {
                BonusType bonusType = static_cast<BonusType> (rand() % BonusType::COUNT_BONUS);
                Bonus *bonus = _gridItemBuilder.buildBonus(i, j, bonusType);
                _scene.addItem(bonus);
            } else if (probability < 0.2) {
                FoodType foodType = static_cast<FoodType> (rand() % FoodType ::COUNT_FOOD);
                Food *food = _gridItemBuilder.buildFood(i, j, foodType);
                _scene.addItem(food);
            }
        }
    }
}

int SceneGraphicsView::get_width() const {
    return _width;
}

void SceneGraphicsView::set_width(int _width) {
    SceneGraphicsView::_width = _width;
}

int SceneGraphicsView::get_height() const {
    return _height;
}

void SceneGraphicsView::set_height(int _height) {
    SceneGraphicsView::_height = _height;
}

void SceneGraphicsView::updateCellAt(QPointF position) {
    auto items = _scene.items(position, Qt::IntersectsItemShape, Qt::DescendingOrder, transform());
    for (auto item : items) {
        Cell *cell = dynamic_cast<Cell *>(item);

        if (cell) {
            Cell *new_cell = _gridItemBuilder.buildCell(cell->get_x(), cell->get_y(), _current_cell_type);

            _scene.removeItem(item);
            _scene.addItem(new_cell);
        }
    }
}

void SceneGraphicsView::updateBonusAt(QPointF position) {
    auto items = _scene.items(position, Qt::IntersectsItemShape, Qt::DescendingOrder, transform());

    Cell *cell = nullptr;
    for (auto item : items) {
        Bonus *bonus = dynamic_cast<Bonus *>(item);
        if (bonus) {
            _scene.removeItem(bonus);
        }

        Food *food = dynamic_cast<Food *>(item);
        if (food) {
            _scene.removeItem(food);
        }

        Cell *curCell = dynamic_cast<Cell *>(item);
        if (curCell) {
            cell = curCell;
        }
    }

    if (cell) {
        if (cell->get_type() == EMPTY_CELL)
            return;

        Bonus *new_cell = _gridItemBuilder.buildBonus(cell->get_x(), cell->get_y(), _current_bonus_type);

        _scene.addItem(new_cell);
    }
}

void SceneGraphicsView::updateFoodAt(QPointF position) {
    auto items = _scene.items(position, Qt::IntersectsItemShape, Qt::DescendingOrder, transform());

    Cell *cell = nullptr;
    for (auto item : items) {
        Bonus *bonus = dynamic_cast<Bonus *>(item);
        if (bonus) {
            _scene.removeItem(bonus);
        }

        Food *food = dynamic_cast<Food *>(item);
        if (food) {
            _scene.removeItem(food);
        }

        Cell *curCell = dynamic_cast<Cell *>(item);
        if (curCell) {
            cell = curCell;
        }
    }

    if (cell) {
        if (cell->get_type() == EMPTY_CELL)
            return;

        Food *new_cell = _gridItemBuilder.buildFood(cell->get_x(), cell->get_y(), _current_food_type);

        _scene.addItem(new_cell);
    }
}

void SceneGraphicsView::mouseReleaseEvent(QMouseEvent *event) {
    QGraphicsView::mouseReleaseEvent(event);

    _mouse_pressed = false;

    if (_current_cell_type != COUNT_CELL)
        updateCellAt(mapToScene(event->pos()));
    if (_current_bonus_type != COUNT_BONUS)
        updateBonusAt(mapToScene(event->pos()));
    if (_current_food_type != COUNT_FOOD)
        updateFoodAt(mapToScene(event->pos()));
}

void SceneGraphicsView::mousePressEvent(QMouseEvent *event) {
    QGraphicsView::mousePressEvent(event);

    _mouse_pressed = true;
}

void SceneGraphicsView::mouseMoveEvent(QMouseEvent *event) {
    QGraphicsView::mouseMoveEvent(event);

    if (!_mouse_pressed) return;

    if (_current_cell_type != COUNT_CELL)
        updateCellAt(mapToScene(event->pos()));
    if (_current_bonus_type != COUNT_BONUS)
        updateBonusAt(mapToScene(event->pos()));
    if (_current_food_type != COUNT_FOOD)
        updateFoodAt(mapToScene(event->pos()));
}

void SceneGraphicsView::set_current_cell_type(CellType _current_type) {
    SceneGraphicsView::_current_cell_type = _current_type;
}

void SceneGraphicsView::set_current_bonus_type(BonusType _current_bonus_type) {
    SceneGraphicsView::_current_bonus_type = _current_bonus_type;
}

void SceneGraphicsView::set_current_food_type(FoodType _current_food_type) {
    SceneGraphicsView::_current_food_type = _current_food_type;
}

QJsonObject SceneGraphicsView::save() {
    std::vector<Cell*> cells;
    std::vector<Bonus*> bonuses;
    std::vector<Food*> foods;

    for (auto item : items()) {
        Cell *cell = dynamic_cast<Cell *>(item);
        if (cell) {
            cells.push_back(cell);
        }

        Bonus *bonus = dynamic_cast<Bonus *>(item);
        if (bonus) {
            bonuses.push_back(bonus);
        }

        Food *food = dynamic_cast<Food *>(item);
        if (food) {
            foods.push_back(food);
        }
    }

    QJsonObject jsonObject;

    QJsonArray cellsArray;
    for (Cell* cell : cells) {
        QJsonObject cellObject;
        cellObject["x"] = cell->get_x();
        cellObject["y"] = cell->get_y();
        cellObject["type"] = (int) cell->get_type();
        cellsArray.push_back(cellObject);
    }

    QJsonArray bonusesArray;
    for (Bonus* bonus : bonuses) {
        QJsonObject cellObject;
        cellObject["x"] = bonus->get_x();
        cellObject["y"] = bonus->get_y();
        cellObject["type"] = (int) bonus->get_type();
        bonusesArray.push_back(cellObject);
    }

    QJsonArray foodArray;
    for (Food* cell : foods) {
        QJsonObject cellObject;
        cellObject["x"] = cell->get_x();
        cellObject["y"] = cell->get_y();
        cellObject["type"] = (int) cell->get_type();
        foodArray.push_back(cellObject);
    }

    jsonObject["width"] = _width;
    jsonObject["height"] = _height;
    jsonObject["cells"] = cellsArray;
    jsonObject["bonuses"] = bonusesArray;
    jsonObject["foods"] = foodArray;

    return jsonObject;
}

void SceneGraphicsView::load(QJsonObject jsonObject) {
    _scene.clear();

    _width = jsonObject["width"].toInt();
    _height = jsonObject["height"].toInt();

    _scene.setSceneRect(- 32, - 32, _width * 32 + 64, _height * 32 + 64 + 32);
    setMinimumWidth(_width * 32 + 32 * 3);

    auto cells = jsonObject["cells"].toArray();
    auto bonuses = jsonObject["bonuses"].toArray();
    auto foods = jsonObject["foods"].toArray();

    for (int i = 0; i < cells.size(); i++) {
        auto cellObject = cells[i].toObject();
        Cell *cell = _gridItemBuilder.buildCell(
                cellObject["x"].toInt(),
                cellObject["y"].toInt(),
                static_cast<CellType>(cellObject["type"].toInt())
        );
        _scene.addItem(cell);
    }

    for (int i = 0; i < bonuses.size(); i++) {
        auto cellObject = bonuses[i].toObject();
        Bonus *cell = _gridItemBuilder.buildBonus(
                cellObject["x"].toInt(),
                cellObject["y"].toInt(),
                static_cast<BonusType >(cellObject["type"].toInt())
        );
        _scene.addItem(cell);
    }

    for (int i = 0; i < foods.size(); i++) {
        auto cellObject = foods[i].toObject();
        Food *cell = _gridItemBuilder.buildFood(
                cellObject["x"].toInt(),
                cellObject["y"].toInt(),
                static_cast<FoodType >(cellObject["type"].toInt())
        );
        _scene.addItem(cell);
    }
}
