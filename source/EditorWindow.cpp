//
// Created by gdhsnlvr on 03.07.17.
//

#include "EditorWindow.h"
#include "LevelSettingDialog.h"

#include <QMenuBar>
#include <QToolBar>
#include <QString>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QtCore/QJsonDocument>
#include <QDebug>

EditorWindow::EditorWindow() : QMainWindow() {

    _mainLayout = new QVBoxLayout();
    _sceneGraphicsView = new SceneGraphicsView();
    _mainLayout->addWidget(_sceneGraphicsView);

    QWidget *centralWidget = new QWidget();
    centralWidget->setLayout(_mainLayout);
    setCentralWidget(centralWidget);

    createMenus();
}

void EditorWindow::resetCurrentType() {
    _sceneGraphicsView->set_current_cell_type(COUNT_CELL);
    _sceneGraphicsView->set_current_bonus_type(COUNT_BONUS);
    _sceneGraphicsView->set_current_food_type(COUNT_FOOD);
}

void EditorWindow::openFileAction() {
    QString fileName = QFileDialog::getOpenFileName(
            this, tr("Save level"), "", tr("Level format json (*.json)")
    );

    if (fileName.isEmpty())
        return;
    else {
        QFile file(fileName);

        if (!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }

        QString str = file.readAll();
        file.close();

        QJsonDocument document = QJsonDocument::fromJson(str.toUtf8());
        _sceneGraphicsView->load(document.object());
    }
}

void EditorWindow::saveFileAction() {
    QString fileName = QFileDialog::getSaveFileName(
            this, tr("Save level"), "", tr("Level format json (*.json)")
    );

    if (fileName.isEmpty())
        return;
    else {
        if (!fileName.contains(".json")) {
            fileName += ".json";
        }

        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }

        QJsonDocument document = QJsonDocument(_sceneGraphicsView->save());
        file.write(document.toJson());
        file.close();
    }
}

void EditorWindow::closeFileAction() {

}

void EditorWindow::generateFileAction() {
    LevelSettingDialog levelSettingDialog;

    if (levelSettingDialog.exec() == QDialog::Accepted) {
        _sceneGraphicsView->set_width(levelSettingDialog.width());
        _sceneGraphicsView->set_height(levelSettingDialog.height());
        _sceneGraphicsView->generate();
    }
}

void EditorWindow::defaultCellAction() {
    resetCurrentType();
    _sceneGraphicsView->set_current_cell_type(DEFAULT_CELL);
}

void EditorWindow::emptyCellAction() {
    resetCurrentType();
    _sceneGraphicsView->set_current_cell_type(EMPTY_CELL);
}

void EditorWindow::movingCellAction() {
    resetCurrentType();
    _sceneGraphicsView->set_current_cell_type(MOVING_CELL);
}

void EditorWindow::surpriseCellAction() {
    resetCurrentType();
    _sceneGraphicsView->set_current_cell_type(SURPRISE_CELL);
}

void EditorWindow::timedCellAction() {
    resetCurrentType();
    _sceneGraphicsView->set_current_cell_type(TIMED_CELL);
}

void EditorWindow::createFileMenu() {
    QMenu *fileMenu = menuBar()->addMenu("File");
    QToolBar *fileToolBar = new QToolBar("File");
    addToolBar(fileToolBar);

    QAction *generateAction = new QAction("New", this);
    QAction *openAction = new QAction("Open", this);
    QAction *saveAction = new QAction("Save", this);
    QAction *closeAction = new QAction("Close", this);

    connect(generateAction, SIGNAL(triggered()), this, SLOT(generateFileAction()));
    connect(openAction, SIGNAL(triggered()), this, SLOT(openFileAction()));
    connect(saveAction, SIGNAL(triggered()), this, SLOT(saveFileAction()));
    connect(closeAction, SIGNAL(triggered()), this, SLOT(closeFileAction()));

    addAction(fileMenu, fileToolBar, generateAction);
    addAction(fileMenu, fileToolBar, openAction);
    addAction(fileMenu, fileToolBar, saveAction);
    addAction(fileMenu, fileToolBar, closeAction);
}

void EditorWindow::healthBonusAction() {
    resetCurrentType();
    _sceneGraphicsView->set_current_bonus_type(HEALTH_BONUS);
}

void EditorWindow::timeSlowBonusAction() {
    resetCurrentType();
    _sceneGraphicsView->set_current_bonus_type(TIME_SLOW_BONUS);
}

void EditorWindow::rocketBonusAction() {
    resetCurrentType();
    _sceneGraphicsView->set_current_bonus_type(ROCKET_BONUS);
}

void EditorWindow::createBonusBar() {
    QMenu *bonusesMenu = menuBar()->addMenu("Bonuses");
    QToolBar *bonusesToolBar = new QToolBar("Bonuses");
    bonusesToolBar->setOrientation(Qt::Vertical);
    addToolBar(Qt::LeftToolBarArea, bonusesToolBar);

    QIcon healthIcon = QIcon(":/resource/health_bonus.png");
    QIcon timeSlowIcon = QIcon(":/resource/time_slow_bonus.png");
    QIcon rocketIcon = QIcon(":/resource/rocket_bonus.png");

    QAction *healthAction = new QAction(healthIcon, "Health bonus", this);
    QAction *timeSlowAction = new QAction(timeSlowIcon, "Time slow bonus", this);
    QAction *rocketAction = new QAction(rocketIcon, "Rocket bonus", this);

    connect(healthAction, SIGNAL(triggered()), this, SLOT(healthBonusAction()));
    connect(timeSlowAction, SIGNAL(triggered()), this, SLOT(timeSlowBonusAction()));
    connect(rocketAction, SIGNAL(triggered()), this, SLOT(rocketBonusAction()));

    addAction(bonusesMenu, bonusesToolBar, healthAction);
    addAction(bonusesMenu, bonusesToolBar, timeSlowAction);
    addAction(bonusesMenu, bonusesToolBar, rocketAction);
}

void EditorWindow::food1FoodAtion() {
    resetCurrentType();
    _sceneGraphicsView->set_current_food_type(FOOD_1);
}

void EditorWindow::food2FoodAtion() {
    resetCurrentType();
    _sceneGraphicsView->set_current_food_type(FOOD_2);
}

void EditorWindow::food3FoodAtion() {
    resetCurrentType();
    _sceneGraphicsView->set_current_food_type(FOOD_3);
}

void EditorWindow::createFoodBar() {
    QMenu *bonusesMenu = menuBar()->addMenu("Foods");
    QToolBar *bonusesToolBar = new QToolBar("Foods");
    bonusesToolBar->setOrientation(Qt::Vertical);
    addToolBar(Qt::LeftToolBarArea, bonusesToolBar);

    QIcon food1Icon = QIcon(":/resource/food_1.png");
    QIcon food2Icon = QIcon(":/resource/food_2.png");
    QIcon food3Icon = QIcon(":/resource/food_3.png");

    QAction *food1Action = new QAction(food1Icon, "Food 1", this);
    QAction *food2Action = new QAction(food2Icon, "Food 2", this);
    QAction *food3Action = new QAction(food3Icon, "Food 3", this);

    connect(food1Action, SIGNAL(triggered()), this, SLOT(food1FoodAtion()));
    connect(food2Action, SIGNAL(triggered()), this, SLOT(food2FoodAtion()));
    connect(food3Action, SIGNAL(triggered()), this, SLOT(food3FoodAtion()));

    addAction(bonusesMenu, bonusesToolBar, food1Action);
    addAction(bonusesMenu, bonusesToolBar, food2Action);
    addAction(bonusesMenu, bonusesToolBar, food3Action);
}

void EditorWindow::createMenus() {
    createFileMenu();
    createCellBar();
    createBonusBar();
    createFoodBar();
}

void EditorWindow::createCellBar() {
    QMenu *cellMenu = menuBar()->addMenu("Cells");
    QToolBar *cellToolBar = new QToolBar("Cells");
    cellToolBar->setOrientation(Qt::Vertical);
    addToolBar(Qt::LeftToolBarArea, cellToolBar);

    QIcon emptyIcon = QIcon(":/resource/empty_cell.png");
    QIcon defaultIcon = QIcon(":/resource/default_cell.png");
    QIcon movingIcon = QIcon(":/resource/moving_cell.png");
    QIcon surpriseIcon = QIcon(":/resource/surprise_cell.png");
    QIcon timedIcon = QIcon(":/resource/timed_cell.png");

    QAction *emptyAction = new QAction(emptyIcon, "Empty cell", this);
    QAction *defaultAction = new QAction(defaultIcon, "Default cell", this);
    QAction *movingAction = new QAction(movingIcon, "Moving cell", this);
    QAction *surpriseAction = new QAction(surpriseIcon, "Surprise cell", this);
    QAction *timedAction = new QAction(timedIcon, "Timed cell", this);

    connect(emptyAction, SIGNAL(triggered()), this, SLOT(emptyCellAction()));
    connect(defaultAction, SIGNAL(triggered()), this, SLOT(defaultCellAction()));
    connect(movingAction, SIGNAL(triggered()), this, SLOT(movingCellAction()));
    connect(surpriseAction, SIGNAL(triggered()), this, SLOT(surpriseCellAction()));
    connect(timedAction, SIGNAL(triggered()), this, SLOT(timedCellAction()));

    addAction(cellMenu, cellToolBar, emptyAction);
    addAction(cellMenu, cellToolBar, defaultAction);
    addAction(cellMenu, cellToolBar, movingAction);
    addAction(cellMenu, cellToolBar, surpriseAction);
    addAction(cellMenu, cellToolBar, timedAction);
}

void EditorWindow::addAction(QMenu *menu, QToolBar *toolBar, QAction *action) {
    menu->addAction(action);
    toolBar->addAction(action);
}
