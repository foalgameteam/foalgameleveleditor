//
// Created by gdhsnlvr on 03.07.17.
//

#include "Cell.h"

#include <QPainter>
#include <QDebug>

Cell::Cell(int x, int y, CellType type, QPixmap *pixmap) : GridItem(x, y, pixmap) {
    _type = type;
}

CellType Cell::get_type() const {
    return _type;
}
