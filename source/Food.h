//
// Created by gdhsnlvr on 03.07.17.
//

#ifndef FOALEDITOR_FOOD_H
#define FOALEDITOR_FOOD_H

#include "GridItem.h"

enum FoodType {
    FOOD_1,
    FOOD_2,
    FOOD_3,
    COUNT_FOOD
};

class Food : public GridItem {
protected:
    FoodType _type;

public:
    Food(int x, int y, FoodType type, QPixmap *pixmap);

    FoodType get_type() const;
};


#endif //FOALEDITOR_FOOD_H
