//
// Created by gdhsnlvr on 03.07.17.
//

#ifndef FOALEDITOR_CELLBUILDER_H
#define FOALEDITOR_CELLBUILDER_H

#include "Cell.h"
#include "Bonus.h"
#include "Food.h"

class GridItemBuilder {
protected:
    std::map<CellType, QPixmap*> _cellImages;
    std::map<BonusType, QPixmap*> _bonusImages;
    std::map<FoodType, QPixmap*> _foodImages;

public:
    GridItemBuilder() = default;

    void setPixmap(CellType type, QPixmap *pixmap);
    void setPixmap(BonusType type, QPixmap *pixmap);
    void setPixmap(FoodType type, QPixmap *pixmap);

    Cell*  buildCell(int x, int y, CellType type);
    Bonus* buildBonus(int x, int y, BonusType type);
    Food*  buildFood(int x, int y, FoodType type);
};

#endif //FOALEDITOR_CELLBUILDER_H
